{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 15,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9320d634-9397-4c7a-93bb-47ba5f65bc88","path":"sprites/spr_bullet_Circle/spr_bullet_Circle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9320d634-9397-4c7a-93bb-47ba5f65bc88","path":"sprites/spr_bullet_Circle/spr_bullet_Circle.yy",},"LayerId":{"name":"5da59c15-48ff-44fd-82e0-caa2d36d41ed","path":"sprites/spr_bullet_Circle/spr_bullet_Circle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bullet_Circle","path":"sprites/spr_bullet_Circle/spr_bullet_Circle.yy",},"resourceVersion":"1.0","name":"9320d634-9397-4c7a-93bb-47ba5f65bc88","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bullet_Circle","path":"sprites/spr_bullet_Circle/spr_bullet_Circle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cb3acb1b-b255-4b8d-a73e-928ef808b979","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9320d634-9397-4c7a-93bb-47ba5f65bc88","path":"sprites/spr_bullet_Circle/spr_bullet_Circle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bullet_Circle","path":"sprites/spr_bullet_Circle/spr_bullet_Circle.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bullet_Circle",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5da59c15-48ff-44fd-82e0-caa2d36d41ed","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Enemy",
    "path": "folders/Sprites/Enemy.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bullet_Circle",
  "tags": [],
  "resourceType": "GMSprite",
}