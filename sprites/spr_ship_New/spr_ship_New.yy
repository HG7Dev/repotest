{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 49,
  "bbox_top": 0,
  "bbox_bottom": 60,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 61,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 1,
  "gridY": 1,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c80fc1b7-ef33-4af5-b44e-684d3f64fc8c","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c80fc1b7-ef33-4af5-b44e-684d3f64fc8c","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"c80fc1b7-ef33-4af5-b44e-684d3f64fc8c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6ad47b5b-705e-4498-8360-6d3bebe39038","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6ad47b5b-705e-4498-8360-6d3bebe39038","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"6ad47b5b-705e-4498-8360-6d3bebe39038","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ce60a0a6-d2a4-479d-ba1e-b8994b87543d","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ce60a0a6-d2a4-479d-ba1e-b8994b87543d","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"ce60a0a6-d2a4-479d-ba1e-b8994b87543d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a45325f8-c4df-48a2-932e-2adf404965b5","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a45325f8-c4df-48a2-932e-2adf404965b5","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"a45325f8-c4df-48a2-932e-2adf404965b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bc003b9b-2db3-46a5-8fc4-b76b242fbdda","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bc003b9b-2db3-46a5-8fc4-b76b242fbdda","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"bc003b9b-2db3-46a5-8fc4-b76b242fbdda","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6912a75e-8d28-47a8-8ce3-750311f07e1a","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6912a75e-8d28-47a8-8ce3-750311f07e1a","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"6912a75e-8d28-47a8-8ce3-750311f07e1a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6757a690-7596-4603-9831-590b4ad303f3","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6757a690-7596-4603-9831-590b4ad303f3","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"6757a690-7596-4603-9831-590b4ad303f3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"062e723a-c55c-456c-819f-04719478a136","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"062e723a-c55c-456c-819f-04719478a136","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"062e723a-c55c-456c-819f-04719478a136","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a58710a5-beff-492b-bc3e-09a38d2dfb1f","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a58710a5-beff-492b-bc3e-09a38d2dfb1f","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"a58710a5-beff-492b-bc3e-09a38d2dfb1f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0fec0775-fdb4-4d15-ab00-6fa4d6755ce0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0fec0775-fdb4-4d15-ab00-6fa4d6755ce0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"0fec0775-fdb4-4d15-ab00-6fa4d6755ce0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c842f16-5d65-456a-aee7-71e644ffbac1","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c842f16-5d65-456a-aee7-71e644ffbac1","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"3c842f16-5d65-456a-aee7-71e644ffbac1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9f91ff5c-8a8d-45e1-81c5-ad07aef1d812","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9f91ff5c-8a8d-45e1-81c5-ad07aef1d812","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"9f91ff5c-8a8d-45e1-81c5-ad07aef1d812","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c2c8ca43-66b6-49a7-a8a1-e843a387b99a","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c2c8ca43-66b6-49a7-a8a1-e843a387b99a","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"c2c8ca43-66b6-49a7-a8a1-e843a387b99a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2881bb8b-eda1-4824-98bb-8a5b35b9c9d0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2881bb8b-eda1-4824-98bb-8a5b35b9c9d0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"2881bb8b-eda1-4824-98bb-8a5b35b9c9d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"751cb281-c9ff-40a9-af71-6d5b08ccb197","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"751cb281-c9ff-40a9-af71-6d5b08ccb197","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"751cb281-c9ff-40a9-af71-6d5b08ccb197","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f14935b-ce6a-4a91-b2dc-92f3d3537ec2","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f14935b-ce6a-4a91-b2dc-92f3d3537ec2","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"2f14935b-ce6a-4a91-b2dc-92f3d3537ec2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aa06141b-c3e4-43cc-b494-2720896d7b4d","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa06141b-c3e4-43cc-b494-2720896d7b4d","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"aa06141b-c3e4-43cc-b494-2720896d7b4d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f8395e3-c6bf-441e-9037-55548bfae62b","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f8395e3-c6bf-441e-9037-55548bfae62b","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"2f8395e3-c6bf-441e-9037-55548bfae62b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cad7e402-f241-4e96-b734-c69a945ed2ac","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cad7e402-f241-4e96-b734-c69a945ed2ac","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"cad7e402-f241-4e96-b734-c69a945ed2ac","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d6b89851-8f63-4fa5-a418-79a271df1d4c","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d6b89851-8f63-4fa5-a418-79a271df1d4c","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"d6b89851-8f63-4fa5-a418-79a271df1d4c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2edfb93a-a8b2-47a1-b372-b57910f1d19b","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2edfb93a-a8b2-47a1-b372-b57910f1d19b","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"2edfb93a-a8b2-47a1-b372-b57910f1d19b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ae4e54dd-25e4-4381-9d9f-fa8a75316595","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ae4e54dd-25e4-4381-9d9f-fa8a75316595","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"ae4e54dd-25e4-4381-9d9f-fa8a75316595","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c58945fe-8e8f-44d9-bb83-7a905a2b82e2","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c58945fe-8e8f-44d9-bb83-7a905a2b82e2","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"c58945fe-8e8f-44d9-bb83-7a905a2b82e2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"026499de-c3f9-4348-b1c4-94aafa1705c5","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"026499de-c3f9-4348-b1c4-94aafa1705c5","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"026499de-c3f9-4348-b1c4-94aafa1705c5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f25324a4-2a90-499c-a472-de018bab69af","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f25324a4-2a90-499c-a472-de018bab69af","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"f25324a4-2a90-499c-a472-de018bab69af","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"de17ea19-516e-4543-a8f7-02c07f246cc0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"de17ea19-516e-4543-a8f7-02c07f246cc0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"de17ea19-516e-4543-a8f7-02c07f246cc0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"081890bc-59a7-4008-acf3-a4054e948aa9","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"081890bc-59a7-4008-acf3-a4054e948aa9","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"081890bc-59a7-4008-acf3-a4054e948aa9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"85c66267-c28e-4d87-907e-707301dd0e75","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"85c66267-c28e-4d87-907e-707301dd0e75","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"85c66267-c28e-4d87-907e-707301dd0e75","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8a90d633-8928-4761-8df9-145fc991eba8","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8a90d633-8928-4761-8df9-145fc991eba8","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"8a90d633-8928-4761-8df9-145fc991eba8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e656e172-7320-480a-a492-08461b248e59","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e656e172-7320-480a-a492-08461b248e59","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"e656e172-7320-480a-a492-08461b248e59","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c6df0f31-97b9-452f-ad9e-279bbdb4b194","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6df0f31-97b9-452f-ad9e-279bbdb4b194","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"c6df0f31-97b9-452f-ad9e-279bbdb4b194","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"174dc663-a8de-4257-abe5-b286f2f751d3","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"174dc663-a8de-4257-abe5-b286f2f751d3","path":"sprites/spr_ship_New/spr_ship_New.yy",},"LayerId":{"name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","name":"174dc663-a8de-4257-abe5-b286f2f751d3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"234909f3-7217-43b4-b30c-ad25217fbf90","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c80fc1b7-ef33-4af5-b44e-684d3f64fc8c","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"643a139c-4c6b-4894-aa64-0f77ee3af997","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6ad47b5b-705e-4498-8360-6d3bebe39038","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9054a346-f77f-442b-8642-f26e58b50e3f","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ce60a0a6-d2a4-479d-ba1e-b8994b87543d","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e5c9aa5-e422-4a71-a036-e9354e093e6a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a45325f8-c4df-48a2-932e-2adf404965b5","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"517a3dea-be0e-461a-9506-2257d8821590","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bc003b9b-2db3-46a5-8fc4-b76b242fbdda","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"20394ce5-9fdb-4873-85c9-bf09bcaa68ad","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6912a75e-8d28-47a8-8ce3-750311f07e1a","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d2b6e220-0550-4b26-8c5d-954af0813b22","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6757a690-7596-4603-9831-590b4ad303f3","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9612369-6fc3-4ace-bf2c-2a0479b006ef","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"062e723a-c55c-456c-819f-04719478a136","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"39b198a5-aaad-4c36-853e-4d2bc0be06a1","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a58710a5-beff-492b-bc3e-09a38d2dfb1f","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a927e5e2-818c-4a43-b8d4-2ef2fb4214f1","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0fec0775-fdb4-4d15-ab00-6fa4d6755ce0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"72258d64-7367-4d42-90f9-63fd982674cc","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c842f16-5d65-456a-aee7-71e644ffbac1","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ddd4f6d6-8b05-4b61-9e1f-03af28866534","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f91ff5c-8a8d-45e1-81c5-ad07aef1d812","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d66e336-41f8-406c-8751-f22835caa223","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c2c8ca43-66b6-49a7-a8a1-e843a387b99a","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1103c832-d338-4b99-9454-f6029ba35740","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2881bb8b-eda1-4824-98bb-8a5b35b9c9d0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c3e3e92-bdba-46a7-889e-19a8cd42ffac","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"751cb281-c9ff-40a9-af71-6d5b08ccb197","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e42c560b-793d-41c9-b7c2-553cfd5ba062","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f14935b-ce6a-4a91-b2dc-92f3d3537ec2","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c25854cb-9d80-4334-8f2a-75bd0bfa9a02","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa06141b-c3e4-43cc-b494-2720896d7b4d","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"227845f1-e62e-44e0-bd7b-3a13fc4081ee","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f8395e3-c6bf-441e-9037-55548bfae62b","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d6fcd3c-8bc5-48ae-bf53-43b044de4384","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cad7e402-f241-4e96-b734-c69a945ed2ac","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"87f6353d-ccdb-4528-a60b-3288f6d5f328","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6b89851-8f63-4fa5-a418-79a271df1d4c","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"798a70bc-772c-4b8f-b92f-14dcadd1c1d0","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2edfb93a-a8b2-47a1-b372-b57910f1d19b","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"67e741b5-6ed3-4c89-8bfc-98106dd850e7","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ae4e54dd-25e4-4381-9d9f-fa8a75316595","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"618bb281-0369-4369-b3d9-999250bcf25c","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c58945fe-8e8f-44d9-bb83-7a905a2b82e2","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8fa2d065-b324-4f36-9a6b-be009a56a307","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"026499de-c3f9-4348-b1c4-94aafa1705c5","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d66ecb4e-8104-417d-8823-9c090007d3b9","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f25324a4-2a90-499c-a472-de018bab69af","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c99d777e-575c-4b60-a911-9afefe5e9c69","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"de17ea19-516e-4543-a8f7-02c07f246cc0","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"115fd698-71be-4c86-9dae-381590b5aac8","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"081890bc-59a7-4008-acf3-a4054e948aa9","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"898246c8-3363-4271-af0a-e712405fe18b","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"85c66267-c28e-4d87-907e-707301dd0e75","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ee3a0fe-50af-44ba-8084-5fe89afdcc4a","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8a90d633-8928-4761-8df9-145fc991eba8","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2a1c77fe-84d9-4907-8b1d-e77191358b67","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e656e172-7320-480a-a492-08461b248e59","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8f66a7d2-6ed9-456a-aab4-f80c908ab962","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6df0f31-97b9-452f-ad9e-279bbdb4b194","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b5b8243e-0f8a-4da4-8f7b-79b7e7c224d8","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"174dc663-a8de-4257-abe5-b286f2f751d3","path":"sprites/spr_ship_New/spr_ship_New.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 30,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ship_New","path":"sprites/spr_ship_New/spr_ship_New.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ship_New",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6544ed17-0bb2-4b5f-bad4-ec8615977190","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ship_New",
  "tags": [],
  "resourceType": "GMSprite",
}