{
  "bboxMode": 0,
  "collisionKind": 0,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 93,
  "bbox_top": 0,
  "bbox_bottom": 123,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 110,
  "height": 124,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9a1b137a-9e4c-473f-94d1-6837b0d07c12","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9a1b137a-9e4c-473f-94d1-6837b0d07c12","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"9a1b137a-9e4c-473f-94d1-6837b0d07c12","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1a2c4503-1e3a-44fc-9984-24ce816d600b","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1a2c4503-1e3a-44fc-9984-24ce816d600b","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"1a2c4503-1e3a-44fc-9984-24ce816d600b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22015d4e-27b3-4b7c-a200-e5471b36644a","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22015d4e-27b3-4b7c-a200-e5471b36644a","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"22015d4e-27b3-4b7c-a200-e5471b36644a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9c5a06f0-1a35-4da0-8257-dce7dbd3783d","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9c5a06f0-1a35-4da0-8257-dce7dbd3783d","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"9c5a06f0-1a35-4da0-8257-dce7dbd3783d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"acac4021-a102-4d1d-8935-04598a010e0e","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"acac4021-a102-4d1d-8935-04598a010e0e","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"acac4021-a102-4d1d-8935-04598a010e0e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"370f374c-2ee9-4f1c-b3c9-2550bd699300","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"370f374c-2ee9-4f1c-b3c9-2550bd699300","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"370f374c-2ee9-4f1c-b3c9-2550bd699300","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"36b44caa-3480-4684-a123-4bde6239102a","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"36b44caa-3480-4684-a123-4bde6239102a","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"36b44caa-3480-4684-a123-4bde6239102a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d3bf94e1-5444-4ec5-93a0-ff61e2aa1b1c","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d3bf94e1-5444-4ec5-93a0-ff61e2aa1b1c","path":"sprites/spr_ship/spr_ship.yy",},"LayerId":{"name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","name":"d3bf94e1-5444-4ec5-93a0-ff61e2aa1b1c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"62ee0207-9801-492e-aa41-890b9eab7a34","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9a1b137a-9e4c-473f-94d1-6837b0d07c12","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc8d04e0-3b36-4b55-b8f3-515ffdb6dc93","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1a2c4503-1e3a-44fc-9984-24ce816d600b","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"18c91bc9-c193-412b-a7fc-23b7ad15534a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22015d4e-27b3-4b7c-a200-e5471b36644a","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a3f05bc-c1e7-4997-8a70-b72ece52dab0","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9c5a06f0-1a35-4da0-8257-dce7dbd3783d","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"77301e56-b278-49e4-950e-5d38339cd825","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"acac4021-a102-4d1d-8935-04598a010e0e","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"64d4262c-96a3-41a9-a07e-f12e3428549c","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"370f374c-2ee9-4f1c-b3c9-2550bd699300","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7cf5e902-ac6c-458c-82f8-25a552b540b8","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"36b44caa-3480-4684-a123-4bde6239102a","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ec0e4d43-85a3-4fd9-b9eb-8addd57ff042","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d3bf94e1-5444-4ec5-93a0-ff61e2aa1b1c","path":"sprites/spr_ship/spr_ship.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 55,
    "yorigin": 62,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ship","path":"sprites/spr_ship/spr_ship.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ship",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a54782b1-20ee-4bd2-a089-8a36822b8b9e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ship",
  "tags": [],
  "resourceType": "GMSprite",
}