{
  "compression": 0,
  "volume": 0.14,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "RoomMusic",
    "path": "audiogroups/RoomMusic",
  },
  "soundFile": "snd_IntroLoop.wav",
  "duration": 32.005,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_IntroLoop",
  "tags": [],
  "resourceType": "GMSound",
}