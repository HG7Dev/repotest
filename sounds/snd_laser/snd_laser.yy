{
  "compression": 0,
  "volume": 0.11,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "SoundEffects",
    "path": "audiogroups/SoundEffects",
  },
  "soundFile": "snd_laser.wav",
  "duration": 0.691621,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_laser",
  "tags": [],
  "resourceType": "GMSound",
}