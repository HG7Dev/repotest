{
  "compression": 0,
  "volume": 0.25,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "SoundEffects",
    "path": "audiogroups/SoundEffects",
  },
  "soundFile": "snd_Explosion.wav",
  "duration": 2.565658,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Explosion",
  "tags": [],
  "resourceType": "GMSound",
}