{
  "compression": 0,
  "volume": 0.6,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "SoundEffects",
    "path": "audiogroups/SoundEffects",
  },
  "soundFile": "snd_overheat.wav",
  "duration": 0.668288,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_overheat",
  "tags": [],
  "resourceType": "GMSound",
}