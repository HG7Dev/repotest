randomize();

waves = ds_list_create();
ds_list_add(waves,[0,oEnemy_Shooter,0,0]);
ds_list_add(waves,[0,oEnemy_Shooter,3,50]);
ds_list_add(waves,[0,oEnemy_Shooter,2,100]);
ds_list_add(waves,[0,oEnemy_Shooter,1,25]);
ds_list_add(waves,[0,oEnemy_Shooter,3,75]);
ds_list_add(waves,[0,oEnemy_Shooter,0,125]);

ds_list_add(waves,[1,oEnemy_Shooter,0,10]);
ds_list_add(waves,[1,oEnemy_Shooter,1,20]);
ds_list_add(waves,[1,oEnemy_Shooter,2,30]);
ds_list_add(waves,[1,oEnemy_Shooter,3,40]);
ds_list_add(waves,[1,oEnemy_Shooter,3,50]);
ds_list_add(waves,[1,oEnemy_Shooter,0,60]);

ds_list_add(waves,[2,oEnemy_Shooter,1,0]);
ds_list_add(waves,[2,oEnemy_Shooter,2,50]);
ds_list_add(waves,[2,oEnemy_Shooter,3,100]);

spawn[0,0] = irandom_range(100,3000);
spawn[0,1] = 600;
spawn[1,0] = irandom_range(2000,3000);
spawn[1,1] = 2500;
spawn[2,0] = irandom_range(100,3000);
spawn[2,1] = 3000;
spawn[3,0] = irandom_range(100,2500);
spawn[3,1] = 800;

