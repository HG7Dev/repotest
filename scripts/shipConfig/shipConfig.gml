// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ship () constructor 
{
	nameof = " ";
	sprite = 0 
	spd = 0 //speed
	HP = 0
	CD = 0 //utility cooldown
}
function ship1 () : ship () constructor 
{
	nameof = "Ship1";
	sprite = spr_ship1
	spd = 3 
	HP = 15
	CD = 30 
}
function ship2 () : ship () constructor 
{
	nameof = "Ship2";
	sprite = spr_ship2
	spd = 6 
	HP = 50
	CD = 60
}