// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function randomDrops(){ //rework later to be used for other random events
	
	var _random = ds_list_create();
	
for (i = 0; i < array_length_1d(global.modList); i++) 
{
	

	var _mod = global.modList[i];
	var _modTier = global.modList[i].tier
	
	switch _modTier
	{
		
		case TIER.D : //Always included in the pool
		ds_list_add(_random,_mod)
		break;
		case TIER.C :
		var _cOdds = random_range(0,100); 
		if _cOdds >= 10 {ds_list_add(_random,_mod)}	//10% chance to not include in pool
		break;
	
	}

}
return _random

}