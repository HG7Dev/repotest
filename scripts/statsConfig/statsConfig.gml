global.gold = 0;


enum STATS { 
	HP,
	SPD ,
	CD, //utility cooldown
	HMAX, //H = heat
	HRATE
}

global.stats[STATS.HP] = 0;
global.stats[STATS.SPD] = 0;
global.stats[STATS.CD] = 0;
global.stats[STATS.HMAX] = 0;
global.stats[STATS.HRATE] = 0;

#macro st global.stats 

#macro sHP STATS.HP
#macro sSPD STATS.SPD
#macro sCD STATS.CD
#macro sHMAX STATS.HMAX
#macro sHRATE STATS.HRATE

