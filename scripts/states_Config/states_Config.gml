// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
enum states
{
	idle,
	wander,
	alert,
	attack,
	inactive,
	pursue,
	still
}

global.myState[states.idle] = enemy_state_idle;
global.myState[states.wander] = enemy_state_wander;
global.myState[states.alert] = enemy_state_alert;
global.myState[states.attack] = enemy_state_attack;
global.myState[states.inactive] = enemy_state_inactive;
global.myState[states.pursue] = enemy_state_pursue;
global.myState[states.still] = enemy_state_still;