
function item () constructor {
	nameof = " ";
	sprite = noone;
	rarity = 0;
}

//////////////////////////////////////////////////////////////////
#region TIER D ITEMS

////////

function HPplus () : item () constructor {
	
	rarity = TIER.D;
	effect = HPplusS;
	desc = "HP +"
	//sprite = spr_  eventually
}
function HPplusS() {
	st[sHP] += 1
}

////////

function SPDplus () : item () constructor {
	
	rarity = TIER.D;
	effect = SPDplusS;
	desc = "Speed +"
	
}
function SPDplusS() {
	st[sSPD] += 1
}

////////

#endregion
//////////////////////////////////////////////////////////////////
#region TIER C ITEMS 

#endregion 
//////////////////////////////////////////////////////////////////