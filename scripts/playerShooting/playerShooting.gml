// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function shoot(slot){	//could include another argument that specifies weapon type,shotgun, machine gun etc.

//is weapon inventory open
if global.displayInv == true exit

//does slot exist
if ds_list_size(global.slots) > slot {

//is weapon off cooldown
if alarm[slot] <= 0 { //everything below
	
	var _dir = direction;
	var _img = image_angle;
	var _x = lengthdir_x(1, _img)
	var _y = lengthdir_y(1, _img)	
	
//is weapon equipped in the slot
if global.slots[|slot] != 0 {  //everything below
	
var _i = 0;
var _p = -5 //angle difference for multiple projectiles i.e. shotgun effect
var _s = 0; //used to even out the spread difference

var weapon = global.slots[|slot];
var _CD = clamp(weapon.cooldown + weapon.mods.cd,10,100);

//repeat for every bullet 
repeat(weapon.mods.pj)
{

	var bullet = instance_create_layer((x+_x+bAdjustX), (y+_y+bAdjustY), "Player", oBullet);
	var _accAdjust = 180 - (weapon.ACC+weapon.mods.ac)*180

	//bullet spread more natural
	if _i == 0
		{ 
			
			var _firstDir =  _dir + random_range(_accAdjust,-1*_accAdjust);
			bullet.direction = _firstDir
			
		}
	else 
		{
			
			if _i mod 2 == 0 
			{ 
				bullet.direction = _firstDir +_s*_p 				
			}		
			
			else
			{
				_s++
				bullet.direction = _firstDir +_s*-_p 
			}
			
		}

	bullet.image_angle = bullet.direction;
	bullet.depth = 301;
	bullet.sprite_index = weapon.projectile;
	
	with oHud 
		{
		slotNum = slot+1;
		cooldownAlpha = .8
		};
	
	bullet.speed = weapon.speed;
	bullet.pierce = weapon.pierce;
	bullet.power = weapon.attackPower+weapon.mods.pw
	
	_i++
	
}
	//heat and cooldown set
	global.heat += clamp(weapon.heatGen + weapon.mods.hg,1,100);
	alarm[slot] = _CD;

}
}
}
}