// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function tiles(layerName,tileSet,tileAmount){
	
randomize();

var tile, tile_tilemap_id, tile_bg;
var tile_x, tile_y, tile_width, tile_height, total_tiles_x, total_tiles_y;
var tile_current;
var tile_data;

 

tile = layer_get_id(layerName);                        //get layer id
tile_tilemap_id = layer_tilemap_get_id(tile);        //get internal tilemap id
tile_bg = tilemap_get_tileset(tile_tilemap_id);            //get tileset resource id

tile_width = tilemap_get_tile_width(tile_tilemap_id);
tile_height = tilemap_get_tile_height(tile_tilemap_id);

total_tiles_x = floor(room_width / tile_width)
total_tiles_y = floor(room_height / tile_height)

var _w = tilemap_get_width(tile_tilemap_id);
var _h = tilemap_get_height(tile_tilemap_id);
total_ = (_w*_h);


tile_x = 0;
tile_y = 0;

do {


   
    if (tile == -1) {tile_bg = "0"};        //if nothing then nothing
   

// ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

tile_current = tilemap_get(tile_tilemap_id, tile_x, tile_y);


        if (tile_bg == tileSet )
        {
			
           var _i = irandom_range(0,tileAmount) //you have to manually set this based on your tile asset
           tilemap_set(tile_tilemap_id, _i, tile_x, tile_y)                
                
           if (round(random(1)) == 1) {      //random chance to mirror tile
			
			tile_data = tilemap_get(tile_tilemap_id,tile_x,tile_y);
			tile_data = tile_set_mirror(tile_data, 1);
			tilemap_set(tile_tilemap_id, tile_data, tile_x, tile_y);
           }
              
         }  
 

// ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
   

     //tile row/collumn counter  
    if (tile_x == total_tiles_x)
    {
        tile_x = 0;
        if (tile_y == total_tiles_y)
        {
            show_debug_message("Something weird happened..");  
        } else {
            tile_y += 1;
        }
    } else {
        tile_x += 1;
    }
 
}

until (tile_x == total_tiles_x && tile_y == total_tiles_y)


}