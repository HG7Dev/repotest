// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function clamp_wrap(variable, minimum, maximum){

if (variable > maximum ) { variable = minimum; }
if (variable < minimum ) { variable = maximum; }
return variable;

}