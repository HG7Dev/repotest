// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function wMod () constructor {
	tier = 0
	sprite = noone
}
function fireRate () : wMod () constructor {
	nameof = "Barrage"
	desc = "Fire Rate +"
	tier = TIER.D
	stat = wCD
	val =5
	sprite = spr_FIRERATE
}
function wPower () : wMod () constructor {
	nameof = "Overload"
	desc = "Power +"
	tier = TIER.D
	stat = wPW
	val =5
	sprite = spr_POWER
}
function wACC () : wMod () constructor {
	nameof = "Scope"
	desc = "Accuracy +"
	tier = TIER.D
	stat = wAC
	val = .02
	sprite = spr_ACCUP
}
function wHeatGen () : wMod () constructor {
	nameof = "Cooling Chip"
	desc = "Heat Generation -"
	tier = TIER.D
	stat = wHG
	val =2
	sprite = spr_HEATGEN
}
function wExtraProj () : wMod () constructor {
	nameof = "Bonus"
	desc = "Extra projectile"
	tier = TIER.C
	stat = wPJ
	val = 1
	sprite = spr_extrashot

}
function fireRate2 () : wMod () constructor {
	nameof = "Mega Barrage"
	desc = "Fire Rate +++"
	tier = TIER.C
	stat = wCD
	val =15
	sprite = spr_FIRERATEPLUS
}
