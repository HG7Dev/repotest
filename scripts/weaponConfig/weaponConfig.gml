// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function weapon () constructor {
	nameof = " ";
	cooldown = 0;
	attackPower = 0;
	sprite = noone;
	pierce = false;
	image_index = 0;
	speed = 0;
	heatGen = 0;
	ACC = 1; //uses 180*ACC to determine range of spread in degrees, for slow weapons <.9 is borderline unusable

	mods = {
					cd : 0 ,//cooldown
					pw : 0 ,//attack power
					hg : 0 ,// heat generation
					ac : 0 ,// accuracy 
					pj : 1 //projectile count
				}

}

function Basic_Laser () : weapon () constructor {
	nameof = "Basic Laser";
	cooldown = 45;
	attackPower = 5;
	sprite = spr_attack;
	pierce = false;
	image_index = 0;
	projectile = spr_bullet;
	speed = 4;
	heatGen = 10
	ACC = .95;


}

function Strong_Laser () : weapon () constructor {
	nameof = "Strong Laser";
	cooldown = 60;
	attackPower = 25;
	sprite = spr_attack;
	pierce = true;
	image_index = 1;
	projectile = spr_bullet_Strong;
	speed = 8;
	heatGen = 20
}

function Bomb () : weapon () constructor {
	nameof = "Bomb";
	cooldown = 90;
	attackPower = 40;
	sprite = spr_attack;
	pierce = false;
	image_index = 2;
	projectile = spr_bomb;
	speed = 5;
	heatGen = 40;
}

function Ice_Bomb () : weapon () constructor {
	nameof = "Ice Bomb";
	cooldown = 120;
	attackPower = 80;
	sprite = spr_attack;
	pierce = true;
	image_index = 3;
	projectile = spr_bomb_Ice;
	speed = 5;
	heatGen = 90
}