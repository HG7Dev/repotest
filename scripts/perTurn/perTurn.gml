//need to set an oldY variable for oVALUEHOLD to push to in the object
//need to set a t value set to 0 for the loop to work 

function pShift(oldY,t){ 

//only integers as coordinates
x = floor(x);
y = floor(y);

//psuedo rotation effect, x and y values
if global.spin != true exit

 //i is the angle, needs to reach 90 degrees by the end of "spin" period, t increases by 1 each frame. 
 //Currently set to 30 frame length in oShip
i = t
	
var _x1 = oShip.x1
var _x2 = oShip.x2
var _y1 = oShip.y1
var _y2 = oShip.y2
			
clamp(y, 0, 1000);	

//oldY is pulled from oVALUEHOLD

if state=states.inactive exit;

if point_in_rectangle( x, y, _x1, _y1-30, _x2, _y2)
{
	
	if i = 90 
	{ 
		topView = false
		yChange = oldY 

	}
	
	depth = 0;		
	midY = _y2-(RES_H*.5);
	distanceToMove = midY-oldY; 

	if i = 0
	{
		yChange = oldY
		totalD = distanceToMove
		topView = true;

	}
	
	

//TOP TO SIDE
if topView == true 
	{
	radians = i *(pi/180)
	sinValue = sin(radians);
	oldY = yChange+(totalD*sinValue);	
	i += 3;

	y = oldY;
	//orignal plan for this was to push the enemy away when shifting so the player doesn't
	//immediately get shot at from point blank if they shifted right next to an enemy
	//having a hard time getting it to work properly though and it looks kind of bad
	//may end up dropping this, OR reworking it to be smoother
	//x = lerp(x, xSpin, _lerpRate);

	}

//this is acting kind of buggy with moving enemies, need to think of the best way to have thing rotate back to a certain point
//having it set as the original point before the shift is proving pretty hard to pull off
//even if I DID implement that well it could result in awkward (and innacurate) huge shifts if the enemy is 
//on the other side of the screen

	
//works great with the asteroids that never change their Y position after the shift
//SIDE TO TOP
else 
	{
		radians = i *(pi/180)
		sinValue = sin(radians);
		adjust = totalD - totalD*sinValue
		oldY = yChange-adjust	

		y = oldY;
		i -=3
	}
	
}
	
//sequence carried out once per frame
t = i;
return t
}
