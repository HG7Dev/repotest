enum TIER {
	D,
	C,
	B,
	A,
	S
}

enum WSTATS {
	POWER,
	COOLDOWN, //effectively fire rate
	HEATGEN,
	ACCURACY,
	PROJECTILE_COUNT
}

#macro wPW WSTATS.POWER
#macro wCD WSTATS.COOLDOWN
#macro wHG WSTATS.HEATGEN
#macro wAC WSTATS.ACCURACY
#macro wPJ WSTATS.PROJECTILE_COUNT

//D mods
global.modList[0] = new fireRate();
global.modList[1] = new wPower();
global.modList[2] = new wACC();
global.modList[3] = new wHeatGen();
//C mods
global.modList[4] = new wExtraProj();
global.modList[5] = new fireRate2();


enum WEAPON {
	B_LASER,
	S_LASER,
	BOMB,
	ICE_BOMB
}

global.weapons[WEAPON.B_LASER] = new Basic_Laser();
global.weapons[WEAPON.S_LASER] = new Strong_Laser();
global.weapons[WEAPON.BOMB] = new Bomb();
global.weapons[WEAPON.ICE_BOMB] = new Ice_Bomb();


