// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function enemy_state_pursue(slow,slowVal){

TargetX = oShip.x - x;
TargetY = oShip.y - y;

var _dist = sqrt(sqr(TargetX)+sqr(TargetY));
var _targetX = TargetX/_dist
var _targetY = TargetY/_dist

if TargetY <= sprite_height/2 and TargetY >= -sprite_height/2 {
	_targetY = 0
}	

if TargetX <= sprite_width/2 and TargetX >= -sprite_width/2 {
	_targetX = 0
}	

if slow == false {
	x+= _targetX
	y+=_targetY;
}

else {
	

var _slowness = slowVal mod 2



	
if _slowness = 0 { //every even frame
	x+= _targetX
	y+=_targetY;

}

slowVal += 1;

}

return slowVal

}