// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function modAdd(_mod,_weapon){ 
	
	switch _mod.stat {
		
		case wCD :
		_weapon.mods.cd -= _mod.val
		break;
		case wPW :
		_weapon.mods.pw += _mod.val
		break;
		case wAC : 
		_weapon.mods.ac += _mod.val
		break;
		case wHG :
		_weapon.mods.hg -= _mod.val
		break;
		case wPJ :
		_weapon.mods.pj += _mod.val
		break;
		
		default :
		show_message("something went wrong");
		break;
		
	}

}