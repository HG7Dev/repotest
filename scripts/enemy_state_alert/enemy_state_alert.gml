function enemy_state_alert(){
//Behavior
	my_dir = point_direction(x, y, oShip.x, oShip.y)
	moveX = lengthdir_x(spd, my_dir);
	moveY = lengthdir_y(spd, my_dir);
	x += moveX;
	y += moveY;
	
	//Transition Triggers
	if(!collision_circle(x,y, 128, oShip, false, false))
	{
		state = states.idle;
	}
	if (collision_circle(x,y, 256, oShip, false, false))
	{
		alarm_set(0, bullet_cooldown);
		if(image_index > image_number-1)
	{
		state = states.wander;
	}
	}
	
	//Spriteeeeeeeeee
	//sprite_index = spr_enemy;
	image_xscale = sign(moveX);
}