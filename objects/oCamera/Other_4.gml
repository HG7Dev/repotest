/// @description

//for some reason this is required on room start up to not break a bunch of camera features
camera = camera_create_view(0,0,RES_W, RES_H);
view_set_camera(0, camera);

follow = oShip; 
shake = 0;

sideViewX = 0;
sideViewY = 0;

tiles("Background02",TileSet2,15);
tiles("Background03",TileSet1,15);

var _lay_id = layer_get_id("Spin");
layer_set_visible(_lay_id,false);

ast = layer_get_id("Asteroids");
near = layer_get_id("Background03");
far = layer_get_id("Background02");
planet = layer_get_id("Background04");

if layer_exists(layer_get_id("Background05")){
spacedust = layer_get_id("Background05");
}

layer_set_visible(planet,false);



