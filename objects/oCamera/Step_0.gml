/// @description
#region parallax

var camX = camera_get_view_x(camera);
var camY = camera_get_view_y(camera);

var lerpNearX = lerp(0,camX,.3)
var lerpNearY = lerp(0,camY,.3);

var lerpMidX = lerp(0,camX,.4)
var lerpMidY = lerp(0,camY,.4);

var lerpFarX = lerp(0,camX, .55)
var lerpFarY = lerp(0,camY,.55);

var lerpVFarX = lerp(0,camX, .6)
var lerpVFarY = lerp(0,camY,.6);


var lerpPX = lerp(layer_get_x(planet),camX, .2);
var lerpPY = lerp(layer_get_y(planet),camY, .2);

layer_x(near, lerpFarX);
layer_y(near, lerpFarY);

layer_x(far, lerpVFarX);
layer_y(far, lerpVFarY);

if layer_exists(layer_get_id("Background05")){
layer_x(spacedust, lerpMidX);
layer_y(spacedust, lerpMidY);
}

if global.sideView == true and global.spin == false 
{
	if layer_get_visible(planet) == false {layer_set_visible(planet, true)}
	
layer_x(planet, lerpPX);
layer_y(planet,lerpPY);

} else 
{ 

	if layer_get_visible(planet) ==  true {layer_set_visible(planet, false)}	
}

//cant figure out how to get parallax effect to work on objects, just tiles, code below doesnt work
if global.sideView = true {

layer_y(ast, lerpNearY);
}


#endregion



