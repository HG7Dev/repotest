/// @description
#macro RES_W 640
#macro RES_H 360

//1080p = 3 , 1440p = 4
scale = 3;

view_enabled = true;
view_visible[0] = true;

camera = camera_create_view(0,0,RES_W*3, RES_H*3);
view_set_camera(0, camera);

window_set_size(RES_W * scale, RES_H* scale);

//PIXEL PERFECT MODE
//surface_resize(application_surface,640,360);

//moved to oManager
//changes the gui res, default is window size
//display_set_gui_size(RES_W, RES_H);

//Center Window
var display_width = display_get_width();
var display_height = display_get_height();
var window_width = RES_W * scale;
var window_height = RES_H * scale;
window_set_position(display_width/2 - window_width/2, display_height/2 - window_height/2);



//follow = oShip; 
shake = 0;

//moved to room start event because different rooms have different layer ID's even if the
//names are the same 
ast = layer_get_id("Asteroids");
near = layer_get_id("Background03");
far = layer_get_id("Background02");
planet = layer_get_id("Background04");
spacedust = layer_get_id("Background05");



surf = -1;


