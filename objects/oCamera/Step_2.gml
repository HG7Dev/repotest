/// @description


var camX = camera_get_view_x(camera);
var camY = camera_get_view_y(camera);

//camera will follow the mouse
if (follow != noone) 
{
var _xTo = window_mouse_get_x();
var _yTo = window_mouse_get_y();

var _xClamp = .5;
var _yClamp = .5;

//cam will be centered on the player when mouse cursor close
if (mouse_x-follow.x >RES_W*.40) {
	_xClamp = .40
}
if (mouse_y-follow.y > RES_H*.40) {
	_yClamp = .40;
}

_xTo = clamp(_xTo, follow.x - RES_W*.60, follow.x - RES_W*_xClamp);
_yTo = clamp(_yTo, follow.y - RES_H*.60, follow.y - RES_H*_yClamp);

}

//prevents camera from exiting room
if global.sideView != true {
_xTo = clamp(_xTo, 0 - RES_W*.02, room_width - RES_W*.98);
_yTo = clamp(_yTo, 0 - RES_H*.03, room_height - RES_H*.97);

camX = lerp(camX, _xTo, .1);
camY = lerp(camY, _yTo, .1);
}

//adjusts camera to side view borders
else {


if sideViewX<0 {sideViewX = 0;}
if sideViewY< 0 {sideViewY = 0}

_xTo = clamp(_xTo, sideViewX-30, sideViewX+30);
_yTo = clamp(_yTo, sideViewY-40, sideViewY+40);

camX = lerp(camX, _xTo, .1);
camY = lerp(camY, _yTo, .1);
}
	

var _shake = power(shake, 2) * 5
camX += random_range(-_shake,_shake);
camY += random_range(-_shake,_shake);
if (shake > 0) shake -= .1;

camera_set_view_pos(camera, camX, camY);