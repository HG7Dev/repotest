/// @description

alarm_set(0,bullet_cooldown);


if global.spin == true exit;
if state != states.pursue exit

var _angle = 0	
repeat(3) {

	
dir = point_direction(x,y,oShip.x,oShip.y)
dir -= 10

var x_offset = lengthdir_x(sprite_width*.5, dir);
var y_offset = lengthdir_y(sprite_width*.5, dir);


bullet = instance_create_layer(x, y, "Enemy", oBullet_Enemy);
bullet.depth = depth+10;
bullet.direction = dir+ 10*_angle;
bullet.image_angle = bullet.direction
bullet.speed = 3;
bullet.sprite_index = spr_bullet_Circle

_angle++;
}

