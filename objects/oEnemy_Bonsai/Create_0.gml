/// @description

// Inherit the parent event
event_inherited();

slow = true;
alert = false;

//stats 
spd = 3;
hp = 50;
hp_max = hp;

//behavior
defaultState = states.still;
state = defaultState;

bullet_cooldown = 150;
alarm_set(0, bullet_cooldown);