// Inherit the parent event
event_inherited();



if  global.spin == false
{
	if global.sideView = true { sprite_index = spr_bonsai }
	else 
	{
		if collision_circle(x,y,sprite_width*5,oShip,false,false)
		{
			alert = true
		}
		if alert = false { image_index = 0 }
	}
}

if global.spin = true 
{
	if global.sideView = false 
	{
		
		if sprite_index != spr_bonsaiSPIN { image_index = 1}
		
		sprite_index = spr_bonsaiSPIN

		if image_index >image_number{ image_speed = 0 };
		else{image_speed = .2}

	}
	
	else 
	{
		sprite_index = spr_bonsaiSPIN
		if image_index < 1  {image_index = image_number-1 }
		if image_index > 1 { image_speed = -.2 };
		else {image_speed = 0} 
	}
	
}