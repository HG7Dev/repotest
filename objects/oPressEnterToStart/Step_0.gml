image_alpha = image_alpha - 0.01;
if (image_alpha <= 0)
{
	image_alpha = 1;
}

if (keyboard_check_pressed(vk_enter))
{
	instance_destroy(oPressEnterToStart);
	instance_create_layer(796, 670, "Menu", oArcade);
	instance_create_layer(796, 670, "Icon", oArcadeIcon);
}