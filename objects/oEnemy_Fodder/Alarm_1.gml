/// @description fire in spurts

if alert == false exit
if global.spin == true exit


dir = point_direction(x,y,oShip.x,oShip.y)
bullet = instance_create_layer(x, y, "Enemy", oBullet_Enemy);

bullet.speed = 3;
bullet.direction = dir;
bullet.image_angle = dir;
bullet.sprite_index = spr_bullet_Circle;

if test = 0{
alarm[1] = 15
}
