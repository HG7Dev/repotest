/// @description
event_inherited();

//experimenting with unique behavior during pShift
//state = states.inactive makes them not be affected by it

test = 0;
//behavior
defaultState = states.inactive; 
state = defaultState;

//alert phase behavior
angle = 0;
length = 1;
alert = false;
p = choose(-1,1)
facing = false;

//stats 
spd = 1;
hp = 15;
hp_max = hp;

attackCD = 30
alarm_set(0, attackCD);