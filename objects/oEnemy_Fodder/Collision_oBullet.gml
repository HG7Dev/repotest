/// @description
/// @description

alert = true;
// damage on only the first frame of collision for piercing attacks
with oCamera {shake = 1};
if other.pierce == false
{	
	with other { instance_destroy();}
	hp -=  other.power
}

else 
{
	pierceCheck += 1;
	if pierceCheck == 1
	{
		hp -= other.power;
	}
}

image_blend = c_red
alarm[3] = 5;	