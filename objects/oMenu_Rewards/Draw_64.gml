/// @description
/// @description

//room clear rewards
if global.roomClear == true
{
	
	draw_sprite(spr_UpgradeBck, -1, RES_W/2, RES_H/2);
	
	for (var i = 0; i < 3; i++) 
	{
		
		var _sprite = choices[i].sprite
		var _space = 8*i; //space between icons
		
		var _x = (xPos) + (i*spriteW)+ _space
		
		draw_text(RES_W/2-60,RES_H/2-50,"CHOOSE YOUR UPGRADE")
		
		draw_sprite(_sprite,-1,_x,yPos);
		draw_sprite(spr_upgradeBorder,-1,_x,yPos);
		
		if itemHover >-1 
		{
			draw_text(RES_W/2-60,RES_H/2-35,choices[itemHover].nameof)
			draw_text(RES_W/2-55,RES_H/2-25,choices[itemHover].desc)
		}

		
	}
	
}