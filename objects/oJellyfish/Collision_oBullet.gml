/// @description
with oCamera {shake = 1};

if other.collided = false or hit = false {
	
hit = true;
var _randDir = choose(-5,-2,0,2,5)
direction = other.direction + _randDir

speed = 10
hp -= other.power;
other.collided = true
}

with other {
	if pierce == false {
		instance_destroy();
	}
}
