/// @description
with oCamera {shake = 2};
effect = instance_create_layer(x,y,"Enemy",oEffect);
effect.sprite_index = spr_explosion

audio_play_sound(snd_Explosion,10,false);

size = 8; //this is how big your chunks will be in pixels

breakApart(sprite_index,size,x,y);