ehealthbar_x = x;
ehealthbar_y = y - 50;

draw_self();
draw_sprite(spr_Healthbarbackground,0,ehealthbar_x,ehealthbar_y);
draw_sprite_stretched(spr_Healthbar,0,ehealthbar_x,ehealthbar_y,(hp/hp_max) * ehealthbar_width, ehealthbar_height);
draw_sprite(spr_Healthbarborder,0,ehealthbar_x,ehealthbar_y);
draw_text(ehealthbar_x,ehealthbar_y, string(hp));