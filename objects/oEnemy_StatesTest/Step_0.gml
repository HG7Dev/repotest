//keeps ship from leaving room
var clamp_value = 12 //how close you can get to the edge of the room
x = clamp(x, clamp_value, room_width-clamp_value);
y = clamp(y, clamp_value, room_height-clamp_value);

script_execute(states_array[state]);