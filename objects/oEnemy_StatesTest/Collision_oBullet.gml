
// damage on only the first frame of collision for piercing attacks
with oCamera {shake = 1};
if other.pierce = false
	{	
	with other {
	instance_destroy();
	}
	hp -=  other.power
	}

if other.pierce 
	{
	frame += 1;
	if frame = 1 
		{
		hp -= other.power;
		alarm[1] = 30
		}
	}
	


if hp<=0 
	{
	instance_destroy();
	}


