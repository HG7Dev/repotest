//draw textbox
draw_sprite_ext(spr_Textbox, 0, x+600,y+800, image_xscale, image_yscale, 0, c_white, 1);


//draw text
if (charCount < string_length(text[page]))
{
	charCount += 1;
}
textPart = string_copy(text[page], 1, charCount);
draw_text_ext(x+600,y+800, textPart, stringHeight, boxWidth);

