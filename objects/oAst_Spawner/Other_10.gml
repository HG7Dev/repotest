/// @description

backLayer = layer_get_id("Background04");
backX = layer_get_x(backLayer);
backY = layer_get_y(backLayer);

oldBackX = backX;
oldBackY = backY;


var astCount = 36
repeat(astCount)
	{
		var choice = choose(0, 1);
	
		if(choice == 0)
		{
			var xx = (random_range(x1, x2));
		}
	
		else
		{
			var xx = (random_range(x1, x2*.8));
		}	
		var yy = (random_range(y2*.9, y2*1.1));
		
		var _ast = instance_create_layer(xx, yy, "Asteroids", oAstSide);
		with _ast {
			wrapX1 = other.x1;
			wrapX2 = other.x2;
		}
	}


sideViewAst = true;

newX1 = x1
newX2 = x2
newY1 = y1
newY2 = y2