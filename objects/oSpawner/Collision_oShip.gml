//WHATS UP, ITS ME, YOUR BOY, TRIGGERED - start waves

if (triggered == false)
{
	//close doors, stop re-trigger
	with (oDoor) closed = true;
	triggered = true;
	
	//# of waves & enemies per wave
	total_waves = -1;
	for (var i = 0; i < ds_list_size(waves); i++)
	{
		var thisentry = ds_list_find_value(waves,i);
		if (thisentry[_WAVE] > total_waves)
		{
			total_waves++;
			remaining[total_waves] = 0;
		}
		remaining[total_waves]++;
		
	}
}
