/// @description
ehealthbar_x = x;
ehealthbar_y = y - 50;
ehealthbar_width = 100;
ehealthbar_height = 12;
//ehealthbar_x = (500/2) - (healthbar_width/2);
//ehealthbar_y = ystart - 100;

draw_self();
draw_sprite(spr_Healthbarbackground,0,ehealthbar_x,ehealthbar_y);
draw_sprite_stretched(spr_Healthbar,0,ehealthbar_x,ehealthbar_y,(hp/hp_max) * ehealthbar_width, ehealthbar_height);
draw_sprite(spr_Healthbarborder,0,ehealthbar_x,ehealthbar_y);
draw_text(ehealthbar_x,ehealthbar_y, string(hp));

var _obj = object_get_name(id.object_index);
if global.sideView == true and _obj == "oEnemy_Flip" {
draw_rectangle(x-10,y-8,x+10,y+10,true);
draw_rectangle(x-15,y-14,x+15,y-8,true);
}