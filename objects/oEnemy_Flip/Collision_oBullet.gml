if state == states.inactive exit
if global.sideView = true {

//weakpoint
if collision_rectangle(x-10,y-8,x+10,y+10,other,false,true) 
{

if other.collided = false 
{
	
hp -= other.power;
image_blend = c_red
alarm[1] = 5;	

	with other 
	{
		if pierce = false {instance_destroy();}
		collided = true;
	}
}
}
//shield hitbox
else if collision_rectangle(x-15,y-14,x+15,y-8,other,false,true) {
	other.direction += choose(150,-150);
	other.image_angle = other.direction;
	other.collided = true;
	}
}

else {
	

#region reflects player projectiles 
if other.collided = false {
switch other.direction {
		case 0 :
		case 90 : 
		case 180 :
		case 270 : 
		other.direction -= 180
		default :
		if other.direction > 45 and other.direction < 135 {
			if other.direction > 90 {other.direction += 90}
			else {other.direction -=90}
		}
		
		else if  other.direction > 225and other.direction < 315 {
			if other.direction > 270 {other.direction += 90}
			else {other.direction -=90}
		}
		
		else if  other.direction > 135 and other.direction < 225 {
			if other.direction > 180 {other.direction += 90}
			else {other.direction -=90}
		}
		
		else if  other.direction > 315 or other.direction < 45 {
			if other.direction <= 45 {other.direction += 90}
			else {other.direction -=90}
		}		
		
		else {
		other.direction -= choose(90,-90);
		}
		other.collided = true;
}
}

other.image_angle = other.direction;
}
#endregion



