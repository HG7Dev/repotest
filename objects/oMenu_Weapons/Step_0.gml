/// @description
/// @description

#region Check if pressed
if keyboard_check_pressed(invButton) {

	if global.displayInv == false {
		global.menuOpen = true;
		global.displayInv = true
		itemNumber = 0
		oldPos = -1;
		oldClicked = -1;
		//equippedWeapon = -1 
		itemNumber =  -1;
		with (oP_Enemy) {
			state = states.inactive }
	} 
	else{	
		
		global.displayInv = false
		global.menuOpen = false;
		
		if global.sideView != true and global.spin != true {
			
			with (oP_Enemy) { 
				state = states.pursue
				}
				
		}
		else {
			var _x1 = oShip.x1
			var _x2 = oShip.x2
			var _y1 = oShip.y1
			var _y2 = oShip.y2
			
			with (oP_Enemy) {
				if point_in_rectangle( x, y, _x1, _y1, _x2, _y2) {
				state = states.pursue
				}

			}
			
			}
			
		}
		
	}


if not global.displayInv exit

#endregion

#region Controls

if oldPos > -1 {
if keyboard_check_pressed(slot1Button) {
global.slots[|0] = oldClicked
oldClicked = -1
oldPos =  -1
			}
				 
else if keyboard_check_pressed(slot2Button) {
global.slots[|1] = oldClicked
oldClicked = -1
oldPos =  -1
			}
				 
else if keyboard_check_pressed(slot3Button) {
global.slots[|2] = oldClicked
oldClicked = -1
oldPos =  -1
			}
}




if mouse_check_button_pressed(mb_left) {
	
	var _mouseX = device_mouse_x_to_gui(0);
	var _mouseY = device_mouse_y_to_gui(0);
	
	if _mouseX < xPos + (invTotal * (iconW*iconAdjust)) and _mouseX >= xPos {
		
		if _mouseY < yPos + iconW and _mouseY >= yPos {
			
		itemNumber = (_mouseX -xPos) div (iconW*iconAdjust);
		var _weaponClicked = global.inventory[|itemNumber];
		var _clickedPos = ds_list_find_index(global.inventory,_weaponClicked);
			
		
		//weapon has been clicked
		if oldPos >=0  {
		
			 if oldPos != _clickedPos
				{
				global.inventory[|oldPos] = _weaponClicked
				global.inventory[|_clickedPos] = oldClicked
				_weaponClicked = -1;
				_clickedPos = -1;
				}
			else {
				//clicked again
				
				oldClicked = -1
				oldPos =  -1
				exit;
				}
			}															
		}
	//if you don't click on a weapon Y VALUE CHECK
	else {exit}	
	}
	//if you don't click on a weapon X VALUE CHECK
	else {exit}	
	//marks the last weapon selected
	oldClicked = _weaponClicked
	oldPos =  _clickedPos
}			
#endregion