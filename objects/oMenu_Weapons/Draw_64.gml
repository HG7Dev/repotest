/// @description
if not global.displayInv exit 

//draw inventory contents
draw_rectangle(xPos-(iconW*.01),150,xPos+(iconW*4.4),250,0)
for (var i = 0; i < invTotal; i++) {	
	var _iconX = xPos +  ((iconW*iconAdjust) * i)
	var _iconY = yPos 
	var _weapon = global.inventory[|i];
	draw_sprite(spr_attack, _weapon.image_index, _iconX, _iconY);
}

//draw selection indication
if itemNumber < invTotal and itemNumber >= 0 {
	if oldPos > -1{
	var _xBox = xPos + ((iconW*1.1) * itemNumber)
	var _yBox = yPos
	draw_sprite(spr_clickedOn,0,_xBox,_yBox);
	}
}