/// @description
if state == states.inactive exit

if other.collided = false {
if global.sideView == false
{

var _damage = clamp(other.power-armor,1,12000);
hp -= _damage

}

else { hp -= other.power*2 }
other.collided = true; // will disable damage for other instances using this check, need to rework 12/7
}

image_blend = c_red
alarm[1] = 5;	

with other {if pierce != true instance_destroy();}
with oCamera {shake = 1};