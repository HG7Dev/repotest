/// @description shooting
direction = 0;

alarm_set(0, attackCD);

if state == states.inactive exit;

if global.spin == false {
if global.sideView == false {

repeat (36) {
direction += 10
dir = direction 
bullet = instance_create_layer(x, y, "Enemy", oBullet_Enemy);
bullet.speed = 3;
bullet.direction = dir;
bullet.image_angle = dir;
bullet.sprite_index = spr_bullet_Circle
image_speed = 0
}
}
else {

for (var i = 0; i<6; i++) {
var _angle = 60 + choose(15,10,20)*i
bullet = instance_create_layer(x, y, "Enemy", oBullet_Enemy);
bullet.direction = _angle;
bullet.image_angle = _angle;
bullet.sprite_index = spr_bullet_Circle
bullet.lob = true;
bullet.oldDirection = _angle;
}
}

}

alarm_set(0, attackCD);