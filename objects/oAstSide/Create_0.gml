/// @description
randomize();
speed = choose (.1, .2);
wrapX1 = 0;
wrapX2 = 0;

//makes smaller asteroids spawn more often than big ones
var astSize = choose(1,2);
if astSize = 1 
{
sprite_index = choose(spr_astBig, spr_astBig2,spr_astSmall, spr_astSmall2);
}
else 
{
	sprite_index = choose(spr_astSmall, spr_astSmall2);
}
direction = choose(0,180);
image_angle = floor(random_range(0, 359 + 1));
