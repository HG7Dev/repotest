/// @description
with oCamera {shake = 2};

score += 5;

//removed particle effects to see if the crash still occurs
repeat(3) 

		{
		var newAsteroid = instance_create_layer(x + 0, y + 0, "Asteroids", oAst);		
		newAsteroid.sprite_index = choose(spr_Cobalt26, spr_astSmall, spr_astSmall2);
		}
		
_x = lengthdir_x(50, image_angle);
_y = lengthdir_y(50, image_angle);

for (var i = 0; i < 3; i ++)
	{					
		switch (i) {
					
				case 0 :
				break;
				case 1 :
				_x = lengthdir_x(50, image_angle+120);
				_y =  lengthdir_y(50, image_angle+120)
				break;
				case 2 :
				_x = lengthdir_x(50, image_angle+240);
				_y =  lengthdir_y(50, image_angle+240);
				break;
				
				}
	var brokenAsteroid = instance_create_layer(x - _x, y - _y, "Asteroids", oAst_Broken);
	var astX = x;
	var astY = y;
	with (brokenAsteroid)
	{
	centerX = astX;
	centerY = astY;
	sprite_index = spr_astBroken;
	speed = .75;
	iAst += i;
	image_index +=i;
	move_towards_point(other.x,other.y,-.5)
	}				
}


effect = instance_create_layer(x,y,"Enemy",oEffect);
effect.sprite_index = spr_explosion

audio_play_sound(snd_Explosion,10,false);

breakApart(sprite_index, 4,x,y);