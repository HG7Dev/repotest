/// @description
if global.sideView == true {
	
	speed = .1
	
	if direction >90 or direction < 270 {
		direction = 180 }
	else { direction = 0 }
} 
else {
direction=astDirection
}
image_angle+=spin;
move_wrap(true, true, sprite_width);

if hp <=0 { instance_destroy();}


if i = 90 {
	if totalD > 0 {
		depth = 500 }
	yChange = y 
	topView = false

}

if global.spin == false exit;
var _x1 = oShip.x1
var _x2 = oShip.x2
var _y1 = oShip.y1
var _y2 = oShip.y2
			
clamp(y, 0, 1000);			

if point_in_rectangle( x, y, _x1, _y1-30, _x2, _y2) {
	

	depth = 0;		
	midY = _y2-(RES_H*.5);
	distanceToMove = midY-oldY;


	
	
if i = 0 {
	yChange = oldY
	totalD = distanceToMove
	topView = true;
}

//moves along the y axis naturally 	
if topView = true{
	
	radians = i *(pi/180)
	sinValue = sin(radians);
	oldY = yChange+(totalD*sinValue);	
	y = oldY;
	i+=3
	if i = 1 { image_index = 1 }
	if i = 20 { image_index = 2 }
	if i =45 { image_index = 3 }
	if i >65 { image_index = 4 }

	}
	else {
	radians = i *(pi/180)
	sinValue = sin(radians);
	adjust = totalD - totalD*sinValue
	oldY = yChange-adjust	
	y = oldY;
	i-=3
	}

}
else {
	visible = false
}