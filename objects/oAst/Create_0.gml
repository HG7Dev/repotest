/// @description
randomize();

oldY = 0;
i = 0;
image_speed = 0;
topView = true;
surf = -1

speed = choose (.5, 1);
spin = choose (.125, -.125, .25,-.25);

//makes smaller asteroids spawn more often than big ones
var astSize = choose(1,2);
if astSize = 1 
{
sprite_index = choose(spr_astBig, spr_astBig2,spr_astSmall, spr_astSmall2);
}
else 
{
	sprite_index = choose(spr_astSmall, spr_astSmall2);
}
astDirection = floor(random_range(0, 359));
image_angle = floor(random_range(0, 359 + 1));

hp = 20;

ww = sprite_get_width(sprite_index); 
hh = sprite_get_height(sprite_index); 