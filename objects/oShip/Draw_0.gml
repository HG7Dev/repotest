/// @description
healthbar_x = x - 30;
healthbar_y = y - 100;

draw_self();
draw_sprite(spr_Healthbarbackground,0,healthbar_x,healthbar_y);
draw_sprite_stretched(spr_Healthbar,0,healthbar_x,healthbar_y,(hp/hp_max) * healthbar_width, healthbar_height);
draw_sprite(spr_Healthbarborder,0,healthbar_x,healthbar_y);
draw_text(healthbar_x,healthbar_y, string(hp));


target = instance_nearest(x,y, oP_Enemy);
if instance_exists(target) {
//disabling for now since I havent done anything with it
//draw_sprite(spr_targeted, 0, target.x,target.y); 
}

if dashing == true {
	var _alarmTime = alarm_get(4);
	
	dashAlpha -= .05
draw_sprite_ext(sprite_index, 0, oldX, oldY,image_xscale,image_yscale,image_angle,c_aqua,dashAlpha)
	
	if _alarmTime == floor(dashLength/2) {
	midDashX = x;
	midDashY = y;
	}
	
	if _alarmTime < floor(dashLength/2) {
	draw_sprite_ext(sprite_index, 0, midDashX, midDashY,image_xscale,image_yscale,image_angle,c_aqua,dashAlpha)
	}
}

