/// @description
if dashCD == true exit;
if hspd == 0 and vspd == 0 exit

dashing = true;
dashCD = true;

var dL = 5 //dash length

var _x = 0;
var _y = 0;

if goingUp = true {
	_y = -dL;
	upDash = true;
}
if goingDown = true {
	_y= dL;
	downDash = true;
}
if goingRight = true {
	_x = dL;
	rightDash = true;
}
if goingLeft = true {
	_x = -dL;
	leftDash = true;
}

diagCheck = diag;
spd = spd*1.5;
diagSpd = floor(spd*((sqrt(2))/2));
spdMax = spd;
diagSpdMax = diagSpd;
dashAlpha = .8;
oldX = x;
oldY = y;


x += _x;
y+= _y;

//frames until you can dash again 
alarm[5] = dashCooldown;

//frames spent dashing
dashLength = 14;
alarm[4] = dashLength



