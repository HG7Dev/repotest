/// @description perspective logic
	x = round(x);
	y = round(y);

image_xscale = 1; 
image_yscale =1;

if transformed = false 
{
	breakApart(sprite_index,8,x,y)
	transformed = true;
	sprite_index = spr_ship_New;
	image_angle = 0;
	image_speed = 1;

	global.sideView = true;
	
	with oCamera 
	{ 
		sideViewX = other.x1;
		sideViewY = other.y1;
	}

}
else if transformed = true
{
	transformed = false;
	image_speed = 0;
	global.sideView = false; 
	
	with (oP_Enemy)
	{
		state = defaultState
		visible = true;
	}

}

global.spin = false;

var _lay_id = layer_get_id("Spin");
layer_set_visible(_lay_id,false);

var lay_id = layer_get_id("Background03");
layer_set_visible(lay_id,true);


transform = false;