if transform = true exit
if dashing == true exit
if justHit == true exit

//will flash red when hit
alarm[2] = 5
image_blend = c_red


hp = hp - 1
alarm[6] = 30
justHit = true

if hp<=0 {
	instance_destroy()
	game_end()
}

with oCamera {shake = 2};
with other { instance_destroy()}
