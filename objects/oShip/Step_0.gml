// if image_index > 15 {image_speed = 0; }	

var _mouseX = device_mouse_x_to_gui(0);
var _mouseY = device_mouse_y_to_gui(0);

floor(x);
floor(y);

hp_max = global.stats[STATS.HP];

if sprite_index = spr_ship_Transform {
	image_xscale += .02;
	image_yscale += .02;	
}


if transform = true exit

if transformed != true {
	
// ship faces the mouse
image_angle = point_direction(x,y,mouse_x,mouse_y)

//keeps ship from leaving room
var clamp_value = 12 //how close you can get to the edge of the room
x = clamp(x, clamp_value, room_width-clamp_value);
y = clamp(y, clamp_value, room_height-clamp_value);
}

direction = point_direction(x,y,mouse_x,mouse_y)


//limits equipped weapons to 3 max
if ds_list_size(global.slots)>3
   {
   while (ds_list_size(global.slots) > 3)
      {
      ds_list_delete(global.slots, 0);
      }
   }






//bAdjust variables adjust bullet creation to track better when the ship is moving
bAdjustX = 0;
bAdjustY =  0;


 //disables movement during a dash
input();

hspd = rkey - lkey;
vspd = dkey - ukey;
if (vspd !=0) and (hspd !=0) {
	diag = true;
}
else { diag = false }


if dashing == false {
	
spd = global.stats[STATS.SPD];
diagSpd = round(spd*((sqrt(2)) / 2));
spdPos = abs(spd);
spdNeg = -spd;

hspd = rkey - lkey;
vspd = dkey - ukey;

if ukey = true { 
	goingUp = true
	bAdjustY = spdNeg}
	else {goingUp = false}
	
if lkey = true { 
	goingLeft = true
	bAdjustY = spdPos;} 
	else {goingLeft = false}
	
if dkey = true {
	goingDown = true
	bAdjustX = spdNeg;} 
	else {goingDown = false}
	
if rkey = true {
	goingRight = true
	bAdjustX = spdPos;} 
	else {goingRight = false}


if global.menuOpen = true exit;
//checks for diagonal input
if diag == true {
x+= hspd *diagSpd;
y+=vspd * diagSpd;
}
else {
x+= hspd *spd;
y+=vspd * spd;
}


}
