/// @description
if transformed = true {
    if direction > 90 and direction < 270
        {
        image_xscale = -1;
        }
		else { image_xscale = 1; image_yscale =1; }
	

//rational numbers cause pixel jitter, this returns integers
	var _x1 = floor(x1)
	var _x2 = floor(x2)
	var _y1 = floor(y1);
	var _y2 = floor(y2);
	
	x = clamp(x, _x1, _x2);
	y = clamp(y,_y1, _y2);
}

