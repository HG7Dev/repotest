/// @description dashing

if dashing == false {
	upDash = false;
	downDash = false;
	leftDash = false;
	rightDash = false;
}

else {

	diagSpd = clamp(diagSpd, 0,diagSpdMax);
	spd = clamp(spd, 0,spdMax);
	
	if diagCheck = true { spd = diagSpd }
	
	if upDash = true { y -= spd}
	if downDash = true { y += spd}
	if leftDash = true { x -= spd}
	if rightDash = true {x += spd}

}
	