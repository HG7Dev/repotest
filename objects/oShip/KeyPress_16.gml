/// @description perspective shift
// move this to a script eventually?

if global.spin==true exit
with (oBullet_Enemy) {
	instance_destroy();
}

var lay_id = layer_get_id("Background03");
layer_set_visible(lay_id,false);
	var cam = view_camera[0];
	 x1 = camera_get_view_x(cam);
	 y1 = camera_get_view_y(cam);
	 x2 = x1 + camera_get_view_width(cam);
	 y2 = y1 + camera_get_view_height(cam);


//in side view
if transformed = true {	

	instance_create_layer(x,y, "Menu", oVALUEHOLD);	
	
	global.spin= true;
	transform = true;
	var dir = image_angle;
	var _x = lengthdir_x(1, dir)
	var _y = lengthdir_y(1, dir)	
	var WOW = instance_create_layer(x+_x,y+_y,"Effects", oEffect);
	WOW.sprite_index = spr_shine;
	WOW.shine = true;
	sprite_index = spr_ship1;
	image_index = 0;
	image_speed = 1;
	alarm[3] = 30; //enemies outside of view get reset here
	
	}
	
	//topview
	else {
		
	instance_create_layer(x,y, "Menu", oVALUEHOLD);	

	var dir = image_angle;
	var _x = lengthdir_x(1, dir)
	var _y = lengthdir_y(1, dir)	
	var WOW = instance_create_layer(x+_x,y+_y,"Effects", oEffect);
	WOW.sprite_index = spr_shine;
	WOW.shine = true;

	global.spin = true;
	transform = true;
	
sprite_index = spr_ship_Transform;
image_index = 0;
image_speed = 1;
image_angle = 270;
alarm[3] = 30;

//fixes pixel jittering 
x = round(x);
y = round(y);

#region checks which enemies to bring 

var _x1 = x1;
var _y1 = y1
var _x2 = x2
var _y2 = y2


//move this shit to the enemy obj
with (oP_Enemy) {
	
if not point_in_rectangle( x, y, _x1, _y1, _x2, _y2) { 	
	state = states.inactive;
	visible = false;
	}
	
else { 
	var push = 100;
	var _dist = abs(other.x -x)
	if _dist < push {_dist = 100 }
	var _playerX = floor(other.x);
	var _playerY = floor(other.y);
	if x > other.x { 
		var _targetX = _playerX + _dist
		}	
	else {
		var _targetX = _playerX - _dist
		}	
	xSpin = floor(_targetX);
	ySpin = floor(_playerY);
		}
	}
	
}
#endregion



if global.spin = true {
var _lay_id = layer_get_id("Spin");
layer_set_visible(_lay_id,true);
}
