/// @description 

sprite_index = global.ship.sprite

//Speed
startSpd = global.ship.spd;
hspd = 0;
vspd = 0;
if st[sSPD] < startSpd {
st[sSPD] = startSpd;
}
spd = global.stats[STATS.SPD];

//HP
startHP = global.ship.HP;
if st[sHP] < startHP {
st[sHP] = startHP;
}
hp = st[sHP];
hp_max = hp;

//Utility
startCD = global.ship.CD
if st[sCD]  < startCD {
st[sCD]  = startCD;
}
dashCooldown = st[sCD] 



heatCD = false;
justHit = false;
dashing = false;
dashCD = false;

//look at this eventually, could remove due to global.spin & g.sideView being added
transformed = false;
transform = false;

bAdjustX = 0;
bAdjustY = 0;

// Bullet cooldown
alarm[0] = 0;
alarm[1] = 0;
alarm[2] = 0;

healthbar_width = 100;
healthbar_height = 12;



