speed = 5;
depth = -1;
power = 0;
pierce = false;
collided = false;


if !audio_is_playing(snd_laser)
{	
	audio_play_sound(snd_laser, 10, false);
}

else if audio_is_playing(snd_laser)
{
	audio_stop_sound(snd_laser)	
	
	if !audio_is_playing(snd_laser)
	{	
		audio_play_sound(snd_laser, 10, false);
	}
}

