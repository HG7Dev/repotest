/// @description
global.displayInv = true;
global.menuOpen = false;

itemNumber = 0

display_set_gui_size(RES_W, RES_H);

guiW = RES_W
guiH = RES_H

weaponSelect = true; //false means ship select

iconW = sprite_get_width(spr_attack)
iconAdjust = 1.1
xPos = RES_W*.1
yPos = RES_H*.75
mouseOver = -4


//assigning these structs inside an array to a variable
//allows modifying the variables inside the struct, when using the variable
//otherwise, the structs either don't update, or they reset themselves, not sure which
//this defeats the point of using the array unless pulling the base stats
//weap1 = new Basic_Laser(); achieves the same thing without the added obfuscation
//leaving this for now 12/6/20
//I think a global array like this might be good for references across objects, just moved this to a new object
//oLoadout and don't have to change any code for it to work 1/10/21
weap1 = global.weapons[WEAPON.B_LASER];
weap2 = global.weapons[WEAPON.S_LASER];
weap3 = global.weapons[WEAPON.BOMB];
weap4 = global.weapons[WEAPON.ICE_BOMB];

//Weapon Inventory
global.inventory = ds_list_create();
ds_list_add(global.inventory, weap1);
ds_list_add(global.inventory, weap2);
ds_list_add(global.inventory, weap3);
ds_list_add(global.inventory, weap4);

invTotal = ds_list_size(global.inventory)
shipTotal = sprite_get_number(spr_shipIcon)

//Equipped Weapons
global.slots = ds_list_create(); //maybe use variables (slot1, slot2, slot3) instead of a list
//ds_list_add(global.slots, weap1);
//ds_list_add(global.slots, weap2);
//ds_list_add(global.slots, weap3);