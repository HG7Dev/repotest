/// @description

//draw inventory contents
draw_set_color(c_orange)
draw_rectangle(xPos-5,RES_H*.95,RES_W*.9,yPos-5,0)
draw_set_color(c_white)

var _x = (guiW/2-iconW)
var _y = (guiH/2-iconW)

if weaponSelect == true
{	
	draw_text_transformed(guiW/2,guiH/4,"CHOOSE YOUR WEAPON",1,1,0)
	
	for (var i = 0; i < invTotal; i++) 
	{	
		var _iconX = xPos +  ((iconW*iconAdjust) * i);
		var _iconY = yPos ;
		var _weapon = global.inventory[|i];
		draw_sprite(spr_attack, _weapon.image_index, _iconX, _iconY);
	}
	
	if mouseOver > -1 && hovering
	{

		var _hover = global.inventory[|mouseOver];
		_hover.sprite_index = spr_attack
		draw_sprite_stretched(_hover.sprite_index, _hover.image_index, _x,_y,iconW*2,iconW*2)
		draw_text(xPos,yPos-50, _hover.nameof)
	}
}
else //ship selection
{
	draw_text_transformed(guiW/2,guiH/4,"CHOOSE YOUR SHIP",2,2,0)
	
	
	for (var i = 0; i < 2; i++) 
	{
		var _iconX = xPos +  ((iconW*iconAdjust) * i);
		var _iconY = yPos ;
		draw_sprite(spr_shipIcon, i, _iconX, _iconY);
	}
	
	if mouseOver > -1 && hovering
	{
		
		draw_sprite_stretched(spr_shipIcon, mouseOver, _x,_y,iconW*2,iconW*2)
		if mouseOver < shipTotal 
		{
		draw_text(xPos,yPos-50, global.ships[mouseOver].nameof)
		}
	}
}


draw_sprite_stretched(spr_clickedOn,0, _x,_y,iconW*2,iconW*2)













