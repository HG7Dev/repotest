/// @description

#region Controls
if weaponSelect
{
	iCount = invTotal
}
else
{
	iCount = shipTotal //ship selection
}
var _mouseX = device_mouse_x_to_gui(0);
var _mouseY = device_mouse_y_to_gui(0);

if _mouseX < xPos + (iCount * (iconW*iconAdjust)) and _mouseX >= xPos 
{
	if _mouseY < yPos + iconW and _mouseY >= yPos 
	{
		
		itemNumber = (_mouseX -xPos) div (iconW*iconAdjust);
		mouseOver = clamp(itemNumber,0,iCount)
		hovering = true;
		
		if mouse_check_button_released(mb_left) 
		{
			if weaponSelect 
			{
				var _weaponClicked = global.inventory[|itemNumber];
				global.slots[|0] = _weaponClicked
				weaponSelect = false;
			}
			else
			{
				global.ship = global.ships[itemNumber] //oShip hasnt been reconfigured so this doesnt do anything
				room_goto_next()
			}
		}
		
	}
// Y VALUE CHECK
	else 
	{	
		hovering = false
	}	
}
//X VALUE CHECK
else 
{	

	hovering = false
}	

#endregion