//Pause Menu
//if (keyboard_check_pressed(ord("P")) to use a key instead of button
if (keyboard_check_pressed(vk_escape))
{
	if (!pause) {
		pause = true;
		instance_deactivate_all(true);
	}
	else
	{
		draw_set_font(fontPixel);
		pause = false;
		instance_activate_all();
	}
}

//Play Music

if room = rm_Start
{
	script_execute(playMusic);
}