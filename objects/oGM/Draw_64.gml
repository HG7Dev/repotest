//Pause Menu
if (pause)
{
	draw_set_color(c_black);
	draw_set_alpha(0.60);
	draw_rectangle(view_xport[0],view_yport[0],view_wport[0],view_hport[0],0);
	draw_set_font(FontMenu);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_text((RES_W)/2,(RES_H)/2,"Paused");
}

//Score
if room != rm_Start
{
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_set_font(FontMenu);
	draw_text(320, 10, string(score));
}
