/// @description
//oManager is persistent so make sure it's only in the first room, otherwise you'll have 2 in the new room
global.menuOpen = false;
global.spin = false;
global.sideView = false;
global.heat = 0

display_set_gui_size(RES_W, RES_H);

hudX = RES_W
hudY = RES_H
hudX = hudX*.5;
hudY = hudY*.9;
once = 0;

global.overheat = false; //first frame of being overheated
heatCD = 5;
alarm[0] = 1;
overheatCD = 120;

enemyCheck = false;