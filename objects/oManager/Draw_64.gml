/// @description
//debug info
draw_set_color(c_lime)
draw_text(0,0, string(fps_real));
var roomname = room_get_name(room);
draw_text(0, 40, roomname);
draw_set_color(c_white);


//Heat Bar
var _width = 100;
var _height = 12;

if global.overheat = true and alarm[1] > 0 {

	var _cd = 1-(alarm[1]/overheatCD)
	
	draw_sprite_stretched_ext(spr_heatbar,0,hudX-50,hudY+25, _width-5 , _height*.5, c_red, 1);
	draw_sprite_stretched_ext(spr_heatbar,0,hudX-50,hudY+25,(_cd) * _width-5, _height*.5, c_lime, 1);
	
}
else { _cd = 0}

draw_sprite(spr_Healthbarbackground,0,hudX-50,hudY);
draw_sprite_stretched_ext(spr_Healthbar,0,hudX-50,hudY,(global.heat/100) * _width, _height, c_white, 1);
draw_sprite(spr_Healthbarborder,0,hudX-50,hudY);
draw_text(hudX-32,hudY-18, string("HEAT"));
draw_text(hudX-4,hudY-18, string(global.heat));


//Mouse reticule
var _mouX = device_mouse_x_to_gui(0);
var _mouY = device_mouse_y_to_gui(0);

if global.displayInv == false {
	draw_sprite(spr_reticule,0,_mouX,_mouY);
}



