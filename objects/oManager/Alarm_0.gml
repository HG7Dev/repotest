/// @description Heat dissipation
switch global.overheat {
	
	case false :
	
		global.heat -= 1;
		alarm[0] = heatCD;
		break;
		
	case true :
	
		alarm[1] = overheatCD;
		break;
}