/// @description
global.heat = clamp(global.heat, 0, 100);

if global.heat >= 100 and global.overheat  = false{
	audio_play_sound(snd_overheat,1,false)
	global.overheat  = true 
}

if instance_exists(oP_Enemy) && enemyCheck == false
{
	enemyCheck = true
}

//pulls up rewards menu when room cleared

if !instance_exists(oP_Enemy) && enemyCheck == true { 
	if once = 0 {
	instance_create_layer(oShip.x+50,oShip.y,"Player",oItem);
	instance_create_depth(0,0,0,oMenu_Rewards); 
	
	once++ }
}