//fadeIn sets the rate the alpha increases
//visual feedback of weapon cooldown
equipTotal = ds_list_size(global.slots)
	
firstSlot = global.slots[|0];
secSlot = global.slots[|1];
thirdSlot = global.slots[|2];

if equipTotal >= 1  {

firstSlotFadeIn = alphaMax/(firstSlot.cooldown + global.slots[|0].mods.cd);
if firstSlotAlpha <1 
{
firstSlotAlpha += clamp(firstSlotFadeIn, 0,1)
};
	
	if equipTotal >= 2 {
	
	secSlotFadeIn = alphaMax/secSlot.cooldown;
	if secSlotAlpha <1
	{
	secSlotAlpha += clamp(secSlotFadeIn, 0,1)
	};
	
		if equipTotal >= 3 {
	
		thirdSlotFadeIn = alphaMax/thirdSlot.cooldown;
		if thirdSlotAlpha <1
		{
		thirdSlotAlpha += clamp(thirdSlotFadeIn, 0,1)
		};
	
}
}
}

	

	

