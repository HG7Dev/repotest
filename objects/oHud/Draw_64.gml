/// @description;
draw_sprite(spr_skillSlots,-1,hudX,hudY);
draw_text(hudX+5,hudY-25,string("LMB"));
draw_text(hudX+40,hudY-25,string("RMB"));
draw_text(hudX+75,hudY-25,string("SPACE BAR"));

firstSlotX = hudX+3;
firstSlotY =  hudY+3;
secSlotX = hudX + 35;
secSlotY = hudY + 3;
thirdSlotX = hudX + 67;
thirdSlotY = hudY + 3;
if slotNum = 1 {
	firstSlotAlpha -= cooldownAlpha
	cooldownAlpha = 0	
	}
if slotNum = 2 {
	secSlotAlpha -= cooldownAlpha
	cooldownAlpha = 0
	}
if slotNum = 3 {
	thirdSlotAlpha -= cooldownAlpha
	cooldownAlpha = 0
}


if equipTotal>=1{

draw_sprite_ext(firstSlot.sprite,firstSlot.image_index, firstSlotX,firstSlotY, 1,1, 0, c_white, firstSlotAlpha);

	if equipTotal>=2 {

	draw_sprite_ext(secSlot.sprite,secSlot.image_index, secSlotX,secSlotY, 1,1, 0, c_white, secSlotAlpha);

		if equipTotal>=3 {
		draw_sprite_ext(thirdSlot.sprite,thirdSlot.image_index, thirdSlotX,thirdSlotY, 1,1, 0, c_white, thirdSlotAlpha);
		}
	}
}