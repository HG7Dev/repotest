//shoots bullet

alarm_set(0, bullet_cooldown);


if global.spin == true exit;
if state == states.inactive exit


dir = point_direction(x,y,oShip.x,oShip.y)
var x_offset = lengthdir_x(16, dir);
var y_offset = lengthdir_y(16, dir);


bullet = instance_create_layer(x+x_offset, y+y_offset, "Enemy", oBullet_Enemy);
bullet.direction = dir;
bullet.image_angle = dir;

