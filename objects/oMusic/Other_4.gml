/// @description Insert description here
// You can write your code in this editor

if room = rm_Start
{
	audio_play_sound(snd_IntroLoop, 1000, true);
}

if room != rm_Start
{
	audio_stop_sound(snd_IntroLoop);
}

if room = rm_testRoom2
{
	audio_play_sound(snd_FirstRoom, 1000, true);
}